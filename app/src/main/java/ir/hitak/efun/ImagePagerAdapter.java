package ir.hitak.efun;

import java.util.ArrayList;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;




public class ImagePagerAdapter extends PagerAdapter {

    public ArrayList<Integer> imageUrls;

    public ImagePagerAdapter(ArrayList<Integer> imageUrls) {
        this.imageUrls = imageUrls;
    }


    @Override
    public int getCount() {
        return imageUrls.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //Typeface typeface= Typeface.createFromAsset(G.context.getAssets(),"IRANSans.ttf");
        View view = G.layoutInflater.inflate(R.layout.sample,null);
        //TextView text = (TextView) view.findViewById(R.id.text);
        ImageView image = (ImageView) view.findViewById(R.id.imageView3);
        //text.setText(imageTitles.get(position));
        //text.setTypeface(typeface);
        Glide.with(G.context).load(imageUrls.get(position)).into(image);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
