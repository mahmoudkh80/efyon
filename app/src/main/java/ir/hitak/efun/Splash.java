package ir.hitak.efun;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;


public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ImageView image = (ImageView) findViewById(R.id.splash_img);
        TextView appName=(TextView)findViewById(R.id.txt_splash);

        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(image);
        Glide.with(this).load(R.mipmap.icon).into(imageViewTarget);

        Typeface typeface=Typeface.createFromAsset(getAssets(),"IRANSansBold.ttf");
        appName.setTypeface(typeface);

        new CountDownTimer(1300, 1) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {


                Intent i = new Intent(Splash.this, MainActivity.class);
                startActivity(i);
                Splash.this.finish();


            }


        }
                .start();


    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitByBackKey();

            //moveTaskToBack(false);

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void exitByBackKey() {

    }
}
