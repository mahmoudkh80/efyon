package ir.hitak.efun.Adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import ir.hitak.efun.DetailBook;
import ir.hitak.efun.G;
import ir.hitak.efun.R;

public class SubAdapter extends RecyclerView.Adapter<SubAdapter.MyViewHolder> {

    private ArrayList<SimpleItem> items;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public CircularImageView imageView;
        public ViewGroup event;
        public String subName;



        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.text_simple);
            imageView =(CircularImageView) view.findViewById(R.id.img);
            event=(ViewGroup) view.findViewById(R.id.event);
            subName ="";



            event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /////////////
                   // G.simpleItems.remove(getAdapterPosition());
                    //CategoryActivity.searchAdapter.notifyItemRemoved(getAdapterPosition());

                    Intent intent=new Intent(G.context, DetailBook.class);
                    intent.putExtra("TITLE",title.getText().toString());
                    intent.putExtra("SUB_NAME",subName);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    G.context.startActivity(intent);



                }
            });

        }
    }

    public SubAdapter(ArrayList<SimpleItem> items) {
        this.items = items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_items, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SimpleItem simple = items.get(position);
        holder.title.setText(simple.title);
        Glide.with(G.context).load(simple.picId).into(holder.imageView);
        holder.subName =simple.category;



    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}