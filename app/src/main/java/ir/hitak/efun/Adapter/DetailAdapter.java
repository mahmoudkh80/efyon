package ir.hitak.efun.Adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import ir.hitak.efun.G;
import ir.hitak.efun.R;
import ir.hitak.efun.SubCategoryActivity;
import ir.hitak.efun.View.DetailView;

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.MyViewHolder> {

    private ArrayList<DetailItem> items;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public DetailView detailView;


        public MyViewHolder(View view) {
            super(view);
           detailView=(DetailView)view.findViewById(R.id.detail_view);


        }
    }

    public DetailAdapter(ArrayList<DetailItem> items) {
        this.items = items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.detail_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        DetailItem simple = items.get(position);
        holder.detailView.setTitle(simple.title);
        holder.detailView.setDescription(simple.description);
        holder.detailView.init();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}