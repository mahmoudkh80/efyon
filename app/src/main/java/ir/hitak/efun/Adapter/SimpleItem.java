package ir.hitak.efun.Adapter;


public class SimpleItem {
    public String title;
    public int picId;
    public String category;

    public SimpleItem(String category, int picId, String title) {
        this.category = category;
        this.picId = picId;
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPicId() {
        return picId;
    }

    public void setPicId(int picId) {
        this.picId = picId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
