package ir.hitak.efun;

import android.content.Context;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import ir.hitak.efun.Adapter.SimpleAdapter;
import ir.hitak.efun.Adapter.SimpleItem;
import ir.hitak.efun.Adapter.SubAdapter;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SubCategoryActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    public static SubAdapter subAdapter;
    private String category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }


        recyclerView = (RecyclerView) findViewById(R.id.sub_category_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.VERTICAL, false));
        //recyclerView.setLayoutManager(new GridLayoutManager(G.context,3));
        subAdapter = new SubAdapter(G.subItems);
        recyclerView.setAdapter(subAdapter);

        Bundle bundle = getIntent().getExtras();

        if (bundle!=null) {
            G.subItems.clear();
            getSupportActionBar().setTitle(bundle.getString("TITLE"));
            G.subTitle=bundle.getString("TITLE");
            category=bundle.getString("CATEGORY");
            Log.i("LOG",category);


            int resId=SubCategoryActivity.this.getResources().getIdentifier(category, "array", SubCategoryActivity.this.getPackageName());
            String[] test = getResources().getStringArray(resId);
            for (String str:test){
                int re=SubCategoryActivity.this.getResources().getIdentifier(str+"_faName", "string", SubCategoryActivity.this.getPackageName());
                int imageId=SubCategoryActivity.this.getResources().getIdentifier(str, "mipmap", SubCategoryActivity.this.getPackageName());
                String b=getResources().getString(re);

                Log.i("LOG",b);

                SimpleItem person=new SimpleItem(str,imageId,b);
                G.subItems.add(person);

            }


        }else{
            getSupportActionBar().setTitle(G.subTitle);

        }


        recyclerView.getAdapter().notifyDataSetChanged();



    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
