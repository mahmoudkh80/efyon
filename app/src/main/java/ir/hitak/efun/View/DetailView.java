package ir.hitak.efun.View;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ir.hitak.efun.G;
import ir.hitak.efun.MainActivity;
import ir.hitak.efun.R;


public class DetailView extends RelativeLayout {
    private TextView title,description;
    private String titleString="",descriptionString="";
    private ImageView arrow,img;
    private CardView cardView;
    private FloatingActionButton fab;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DetailView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(context);
    }

    public DetailView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    public DetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public DetailView(Context context) {
        super(context);
        initialize(context);
    }
    public void initialize(final Context context){
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=inflater.inflate(R.layout.detail_view,DetailView.this,true);

        title=(TextView)view.findViewById(R.id.detail_title);
        description=(TextView)view.findViewById(R.id.detail_description);
        arrow=(ImageView)view.findViewById(R.id.detail_img);
        img=(ImageView)view.findViewById(R.id.imageView4);
        cardView=(CardView) view.findViewById(R.id.detail_card);
        fab=(FloatingActionButton) view.findViewById(R.id.fab_share);

        arrow.setImageResource(R.drawable.ic_arrow_down);
        cardView.setVisibility(View.GONE);

        arrow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                 if (cardView.getVisibility()==View.GONE){
                     arrow.setImageResource(R.drawable.ic_arrow_up);
                     cardView.setVisibility(View.VISIBLE);

                 }else{
                     arrow.setImageResource(R.drawable.ic_arrow_down);
                     cardView.setVisibility(View.GONE);
                 }
            }
        });
        img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cardView.getVisibility()==View.GONE){
                    arrow.setImageResource(R.drawable.ic_arrow_up);
                    cardView.setVisibility(View.VISIBLE);

                }else{
                    arrow.setImageResource(R.drawable.ic_arrow_down);
                    cardView.setVisibility(View.GONE);
                }
            }
        });
        fab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String applink="«افیون» را در بازار اندروید ببین:\n" +
                        "http://cafebazaar.ir/app/?id=com.hmz.tarkemavaad&ref=share";

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, description.getText().toString().substring(0,200)+" ... "+"\n\n\n"+applink);
                sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                getContext().startActivity(Intent.createChooser(sharingIntent, "اشتراک گذاری با :"));
            }
        });

        init();

    }
    public void setTitle(String title){
        this.titleString=title;
    }
    public void setDescription(String description){
        this.descriptionString=description;
    }
    public void init(){
        title.setText(titleString);
        description.setText(descriptionString);
    }

}
