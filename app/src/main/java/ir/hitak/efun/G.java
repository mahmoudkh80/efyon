package ir.hitak.efun;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;

import java.util.ArrayList;

import ir.hitak.efun.Adapter.DetailItem;
import ir.hitak.efun.Adapter.SimpleItem;
import ir.hitak.efun.Adapter.SubAdapter;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class G extends Application {
    public static Context context;
    public static ArrayList<SimpleItem> simpleItems =new ArrayList<>();
    public static ArrayList<SimpleItem> subItems =new ArrayList<>();
    public static ArrayList<SimpleItem> searchItems =new ArrayList<>();
    public static ArrayList<DetailItem> detailItems =new ArrayList<>();
    public static String subTitle;
    public static LayoutInflater layoutInflater;


    @Override
    public void onCreate() {
        super.onCreate();
        context=getApplicationContext();
        layoutInflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        FontsOverride.setDefaultFont(this, "MONOSPACE", "IRANSans.ttf");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("IRANSans.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


    }
}
