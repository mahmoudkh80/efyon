package ir.hitak.efun;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import ir.hitak.efun.Adapter.DetailAdapter;
import ir.hitak.efun.Adapter.DetailItem;
import ir.hitak.efun.Adapter.SimpleAdapter;
import ir.hitak.efun.Adapter.SimpleItem;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailBook extends AppCompatActivity {
    private RecyclerView recyclerView;
    public static DetailAdapter detailAdapter;
    private String subName;
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_book);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        img = (ImageView) findViewById(R.id.imageView3);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }



        recyclerView = (RecyclerView) findViewById(R.id.detail_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.VERTICAL, false));
        //recyclerView.setLayoutManager(new GridLayoutManager(G.context,3));
        detailAdapter = new DetailAdapter(G.detailItems);
        recyclerView.setAdapter(detailAdapter);

        Bundle bundle=getIntent().getExtras();
        G.detailItems.clear();
        if (bundle!=null){
            G.detailItems.clear();
            subName=bundle.getString("SUB_NAME");
            Log.e("LOG",subName);
            int titleId=DetailBook.this.getResources().getIdentifier(subName+"_faName", "string", DetailBook.this.getPackageName());
            int imageId=DetailBook.this.getResources().getIdentifier(subName, "mipmap", DetailBook.this.getPackageName());

            img.setImageResource(imageId);

            getSupportActionBar().setTitle(getResources().getString(titleId));




            int moarefiId=DetailBook.this.getResources().getIdentifier(subName+"_moarefi", "string", DetailBook.this.getPackageName());
            int tarikhcheId=DetailBook.this.getResources().getIdentifier(subName+"_tarikhche", "string", DetailBook.this.getPackageName());
            int anvaId=DetailBook.this.getResources().getIdentifier(subName+"_anva", "string", DetailBook.this.getPackageName());
            int masrafId=DetailBook.this.getResources().getIdentifier(subName+"_masraf", "string", DetailBook.this.getPackageName());
            int avarezId=DetailBook.this.getResources().getIdentifier(subName+"_avarez", "string", DetailBook.this.getPackageName());


            G.detailItems.add(new DetailItem("معرفی",getResources().getString(moarefiId)));
            G.detailItems.add(new DetailItem("تاریخچه",getResources().getString(tarikhcheId)));
            G.detailItems.add(new DetailItem("انواع",getResources().getString(anvaId)));
            G.detailItems.add(new DetailItem("مصرف",getResources().getString(masrafId)));
            G.detailItems.add(new DetailItem("عوارض",getResources().getString(avarezId)));

        }






        recyclerView.getAdapter().notifyDataSetChanged();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
