package ir.hitak.efun;

import android.app.SearchManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ir.hitak.efun.Adapter.SearchAdapter;
import ir.hitak.efun.Adapter.SimpleItem;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    public static SearchAdapter searchAdapter;
    private ArrayList<String> strings = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }

        recyclerView = (RecyclerView) findViewById(R.id.search_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.VERTICAL, false));
        //recyclerView.setLayoutManager(new GridLayoutManager(G.context,3));
        searchAdapter = new SearchAdapter(G.searchItems);
        recyclerView.setAdapter(searchAdapter);

        /** G.subItems.clear();
         Log.i("LOG",category);


         int resId=SearchActivity.this.getResources().getIdentifier(category, "array", SearchActivity.this.getPackageName());
         String[] test = getResources().getStringArray(resId);
         for (String str:test){
         int re=SearchActivity.this.getResources().getIdentifier(str+"_faName", "string", SearchActivity.this.getPackageName());
         String b=getResources().getString(re);

         Log.i("LOG",b);

         SimpleItem person=new SimpleItem(str,R.mipmap.header,b);
         G.subItems.add(person);

         }
         **/


        G.searchItems.clear();
        String[] maintitle = getResources().getStringArray(R.array.main_title);

        for (String string : maintitle) {
            int main_title_str = SearchActivity.this.getResources().getIdentifier(string + "_faName", "string", SearchActivity.this.getPackageName());
            String title = getResources().getString(main_title_str);


            int resId = SearchActivity.this.getResources().getIdentifier(string, "array", SearchActivity.this.getPackageName());
            String[] test = getResources().getStringArray(resId);
            for (String str : test) {
                int re = SearchActivity.this.getResources().getIdentifier(str + "_faName", "string", SearchActivity.this.getPackageName());
                int imageId=SearchActivity.this.getResources().getIdentifier(str, "mipmap", SearchActivity.this.getPackageName());
                String b = getResources().getString(re);

                //Log.i("LOG",b);
                strings.add(b);
                Log.e("LOG", str);

                SimpleItem person = new SimpleItem(str,imageId, b);
                G.searchItems.add(person);
                recyclerView.getAdapter().notifyDataSetChanged();

            }


        }




        recyclerView.getAdapter().notifyDataSetChanged();

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("جستجو");
        searchView.setFocusable(false);
        searchMenuItem.expandActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                query = query.toLowerCase();

                final List<SimpleItem> filteredList = new ArrayList<>();

                for (int i = 0; i < G.searchItems.size(); i++) {

                    final String text = G.searchItems.get(i).getTitle().toLowerCase();
                    if (text.contains(query)) {

                        filteredList.add(G.searchItems.get(i));
                    }
                }

                recyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
                searchAdapter = new SearchAdapter((ArrayList<SimpleItem>) filteredList);
                recyclerView.setAdapter(searchAdapter);
                searchAdapter.notifyDataSetChanged();  // data set changed

                return true;
            }
        });
        return true;
    }



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }



}
