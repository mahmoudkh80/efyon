package ir.hitak.efun;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.ravindu1024.indicatorlib.ViewPagerIndicator;

import java.util.ArrayList;

import ir.hitak.efun.Adapter.SimpleAdapter;
import ir.hitak.efun.Adapter.SimpleItem;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CategoryActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    public static SimpleAdapter simpleAdapter;
    private ViewPager pager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }

        recyclerView = (RecyclerView) findViewById(R.id.category_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.VERTICAL, false));
        //recyclerView.setLayoutManager(new GridLayoutManager(G.context,3));
        simpleAdapter = new SimpleAdapter(G.simpleItems);
        recyclerView.setAdapter(simpleAdapter);

        ArrayList<Integer> imageUrls=new ArrayList<>();
        imageUrls.add(R.drawable.i1);
        imageUrls.add(R.drawable.i2);
        imageUrls.add(R.drawable.i3);
        imageUrls.add(R.drawable.i4);
        imageUrls.add(R.drawable.i5);

        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        ImagePagerAdapter adapter = new ImagePagerAdapter(imageUrls);
        pager.setAdapter(adapter);
        pager.setCurrentItem(2);

        ViewPagerIndicator indicator = (ViewPagerIndicator) findViewById(R.id.pager_indicator);
        indicator.setPager(pager);  //pager - the target ViewPager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            indicator.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }


        G.simpleItems.clear();
        String[] maintitle = getResources().getStringArray(R.array.main_title);

        for (String string : maintitle){
            int main_title_str=CategoryActivity.this.getResources().getIdentifier(string+"_faName", "string", CategoryActivity.this.getPackageName());
            int imageId=CategoryActivity.this.getResources().getIdentifier(string, "mipmap", CategoryActivity.this.getPackageName());
            String title=getResources().getString(main_title_str);
            Log.e("LOG",title);

            SimpleItem simpleItem =new SimpleItem(string,imageId,title);
            G.simpleItems.add(simpleItem);


        }


        recyclerView.getAdapter().notifyDataSetChanged();





    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
